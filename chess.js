var chessField = function () {
    var line = 8;
    
    var simbolBlack = '██';
    var simbolWhite = '  ';
    var string = '';
    
    for (var i = 0; i < line; i++) {
        for (var j = 0; j < line; j++) {
            if ((i % 2 === 0 && j % 2 === 0) || (i % 2 !== 0 && j % 2 !== 0)) {
                string += simbolBlack;
            } else {
                string += simbolWhite;
            }
        }
        string += '\n';
    }
    console.log(string);
};

chessField();